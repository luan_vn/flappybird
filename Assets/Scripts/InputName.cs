﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InputName : MonoBehaviour
{
    [SerializeField] private TMP_InputField inputF;
    public void SaveName()
    {
        GameManager.Intance.NamePlayer = inputF.text;
    }
}
