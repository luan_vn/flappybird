﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseCode.Utils;

public class MainMenu : MonoBehaviour {
    [SerializeField] private InputName inputName;
    private void Awake() {
        transform.Find("playBtn").GetComponent<Button_UI>().ClickFunc = () => { inputName.SaveName(); Loader.Load(Loader.Scene.GameScene); };
        transform.Find("playBtn").GetComponent<Button_UI>().AddButtonSounds();

        transform.Find("quitBtn").GetComponent<Button_UI>().ClickFunc = () => { Application.Quit(); };
        transform.Find("quitBtn").GetComponent<Button_UI>().AddButtonSounds();
    }

}

