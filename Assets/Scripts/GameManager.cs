﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager intance;

    public static GameManager Intance => intance;

    public string NamePlayer;

    private void Awake()
    {
        if (intance == null)
        {
            intance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }
}
